![Docker 下載](https://img.shields.io/docker/pulls/xiuxiumycena/whisper-asr-webservice.svg)
![許可證](https://img.shields.io/gitlab/license/hengshiousheu/whisper-asr-webservice.svg)

# Whisper ASR Web 服務
Whisper 是一個通用的語音識別模型。它訓練於一個包含多樣化音頻的大型數據集上，同時也是一個多任務模型，能夠進行多語言語音識別以及語音翻譯和語言識別。更多詳情：[github.com/openai/whisper](https://github.com/openai/whisper/)

## 特色
當前發行版本（v1.3.0）支持以下 whisper 模型：

- [openai/whisper](https://github.com/openai/whisper)@[v20231117](https://github.com/openai/whisper/releases/tag/v20231117)
- [SYSTRAN/faster-whisper](https://github.com/SYSTRAN/faster-whisper)@[v0.10.0](https://github.com/SYSTRAN/faster-whisper/releases/tag/0.10.0)

## 快速使用

### CPU

```sh
docker run -d -p 9000:9000 -e ASR_MODEL=base -e ASR_ENGINE=faster_whisper xiuxiumycena/whisper-asr-webservice:0.0.1
```

### GPU
僅支持 amd64 架構

```sh
docker run -d --gpus all -p 9000:9000 -e ASR_MODEL=base -e ASR_ENGINE=faster_whisper xiuxiumycena/openai-whisper-asr-webservice:0.0.1-gpu
```

arm64 - jetson 架構還在持續 build 當中
```sh
docker run -d --gpus all -p 9000:9000 -e ASR_MODEL=base -e ASR_ENGINE=faster_whisper xiuxiumycena/openai-whisper-asr-webservice:0.0.1-gpu-jetson
```

## 本地開發

首先，確保系統上安裝了 Python 3.10。你可以使用 `pyenv` 或其他版本管理工具來管理 Python 版本。

### 設置環境(Doc)
1. **Install poetry with following command:**:
```sh
pip3 install poetry
```

2. Install packages:
```sh
poetry install
```

3. Starting the Webservice:
```sh
poetry run gunicorn --bind 0.0.0.0:9000 --workers 1 --timeout 0 app.webservice:app -k uvicorn.workers.UvicornWorker
```

### 設置環境
1. **安裝 Poetry**：
    如果尚未安裝 Poetry，可以使用以下指令安裝：
    ```sh
    curl -sSL https://install.python-poetry.org | python3 -
    ```

2. **設定虛擬環境**：
    將虛擬環境設置在專案目錄中：
    ```sh
    poetry config virtualenvs.in-project true
    ```

3. **指定 Python 版本並創建虛擬環境**：
    確保 Poetry 使用 Python 3.10 來創建虛擬環境：
    ```sh
    poetry env use 3.10
    ```

4. **安裝依賴**：
    安裝專案的所有依賴：
    ```sh
    poetry install
    ```

5. **進入虛擬環境**：
    啟動虛擬環境：
    ```sh
    poetry shell
    ```

6. **確認 Python 版本**：
    確保虛擬環境中使用的是正確的 Python 版本：
    ```sh
    python --version  # 應該顯示 Python 3.10.x
    ```

這樣可以確保開發環境配置正確，並且使用指定的 Python 版本。

## 測試
要運行單元測試，使用以下命令：
```
poetry run pytest

```

## 本地 Docker 開發
確認是在 poetry 環境後
```bash
 docker-compose -f docker-compose.yml up
```

## Docker 封裝指令

amd64/arm64 架構用<CPU>
```bash
 DOCKER_BUILDKIT=1 docker buildx build --platform linux/arm64 -f Dockerfile. -t xiuxiumycena/whisper-asr-webservice:0.0.1 . --push
```

amd64 架構用<GPU>
```bash
 DOCKER_BUILDKIT=1 docker buildx build --platform linux/arm64 -f Dockerfile. -t xiuxiumycena/whisper-asr-webservice:0.0.1-gpu . --push
```

Arm64 Jetson 架構用
```bash
 DOCKER_BUILDKIT=1 docker buildx build --platform linux/arm64 -f Dockerfile.gpu.jetson -t xiuxiumycena/whisper-asr-webservice:0.0.1-gpu-jetson . --push
```

更多信息:

- [Documentation/Run](https://ahmetoner.github.io/whisper-asr-webservice/run)
- [Docker Hub](https://hub.docker.com/r/onerahmet/openai-whisper-asr-webservice)

## 文檔
Explore the documentation by clicking [here](https://ahmetoner.github.io/whisper-asr-webservice).

## 鳴謝
- 本專案使用 [FFmpeg](http://ffmpeg.org) 項目下的庫，遵循 [LGPLv2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) 許可證
- 本專案參照 [ahmetoner](https://github.com/ahmetoner/whisper-asr-webservice)進行調整
