import os
from io import StringIO
from threading import Lock
from typing import Union, BinaryIO, List
from collections.abc import Iterable

import torch
from app.core.audio import pad_or_trim
from faster_whisper import WhisperModel
from fastapi import HTTPException
from .utils import ResultWriter, WriteTXT, WriteSRT, WriteVTT, WriteTSV, WriteJSON
from loguru import logger

model_name = os.getenv("ASR_MODEL", "base")
model_path = os.getenv("ASR_MODEL_PATH", os.path.join(os.path.expanduser("~"), ".cache", "whisper"))

# More about available quantization levels is here:
#   https://opennmt.net/CTranslate2/quantization.html
if torch.cuda.is_available():
    device = "cuda"
    model_quantization = os.getenv("ASR_QUANTIZATION", "float32")
else:
    device = "cpu"
    model_quantization = os.getenv("ASR_QUANTIZATION", "int8")

model = WhisperModel(
    model_size_or_path=model_name,
    device=device,
    compute_type=model_quantization,
    download_root=model_path
)

model_lock = Lock()

class ProhibitedContentChecker:
    def __init__(self):
        self.prohibited_content = {
            "en": ["Thanks for watching!", "Thank you for watching!"],
            "zh": ["感謝觀看", "謝謝觀看 歡迎訂閱我的頻道", "字幕志願者 李宗盛", "字幕由Amara.org社區提供", "請不吝點贊訂閱轉發打賞支持明鏡與點點欄目", "作詞", "輪迴來", "字幕製作人", "請按旁邊的小鈴鐺", "這次的影片就到這裡了", "編曲"],
            "ja": ["おはようございます。", "ご視聴ありがとうございました", "【拍手】"],
            "ko": ["구독과 좋아요 부탁드립니다", "수고하셨습니다", "고맙습니다", "시청해주셔서", "감사합니다"],
            "id": ["Terima kasih telah menonton!", "Terima kasih.", "Sampai jumpa di video selanjutnya.", "Terima kasih sudah menonton!"],
            "th": ["โปรดติดตามตอนต่อไป", "สสนะส Age 1 ชาย ทั้งไหม", "리้าใช่ โทษว์ หมชาติ", "โอเค", "ขอบคุณ", "อืม"],
            "vi": ["Các bạn hãy đăng kí cho kênh lalaschool Để không bỏ lỡ những video hấp dẫn", "Hãy subscribe cho kênh Ghiền Mì Gõ Để không bỏ lỡ những video hấp dẫn", "Được rồi", "Để không bỏ lỡ lỡ lỡ lỡ lỡ", "Được rồi"]
        }

    def check(self, transcription: str, language: str) -> bool:
        """
        检查转录结果中是否包含禁止的字符串。
        """
        prohibited_strings = self.prohibited_content.get(language, [])
        for string in prohibited_strings:
            if string.strip() in transcription:
                return True
        return False

checker = ProhibitedContentChecker()

# 初始化腳本數據庫和匹配器
from .utils import ScriptDatabase, FlexibleScriptMatcher
script_db = ScriptDatabase()
script_matcher = FlexibleScriptMatcher(script_db, similarity_threshold=0.65)

def transcribe(
        audio,
        task: Union[str, None],
        language: Union[str, None],
        initial_prompt: Union[str, None],
        vad_filter: Union[bool, None],
        word_timestamps: Union[bool, None],
        hotwords: Union[str, None],
        output,
):
    options_dict = {"task": task}
    if language:
        options_dict["language"] = language
    if initial_prompt:
        options_dict["initial_prompt"] = initial_prompt
    if vad_filter:
        options_dict["vad_filter"] = True
    if word_timestamps:
        options_dict["word_timestamps"] = True
    
    # 從環境變數獲取額外的hotwords
    env_hotwords = os.environ.get('HOTWORDS')
    
    # 合併所有來源的hotwords
    combined_hotwords = merge_hotwords(hotwords, env_hotwords)
    
    # 只有在有hotwords時才添加到選項中
    if combined_hotwords:
        options_dict["hotwords"] = combined_hotwords
    
    with model_lock:
        segments = []
        text = ""
        segment_generator, info = model.transcribe(audio, beam_size=5, **options_dict)
        for segment in segment_generator:
            segments.append(segment)
            text = text + segment.text
        
        logger.info(f"要求 Language [{info.language}], 取得錄音文字{text}")

        # 檢查是否匹配腳本
        detected_language = options_dict.get("language", info.language)
        matched_script, similarity_score = script_matcher.find_matching_script(text.strip(), detected_language)

        # 如果找到匹配的腳本，使用腳本內容替換轉錄結果
        if matched_script:
            logger.info(f"找到匹配腳本 (相似度: {similarity_score:.2f}): {matched_script}")
            text = matched_script
            segments = [{
                "text": matched_script,
                "start": segments[0].start if segments else 0,
                "end": segments[-1].end if segments else 0,
            }]
        else:
            logger.info(f"未找到匹配腳本 (最高相似度: {similarity_score:.2f}, 無匹配文字: {matched_script})")

        result = {
            "language": detected_language,
            "segments": segments,
            "text": text
        }

    output_file = StringIO()
    write_result(result, output_file, output)
    output_file.seek(0)



    return output_file


def language_detection(audio):
    # load audio and pad/trim it to fit 30 seconds
    audio = pad_or_trim(audio)

    # detect the spoken language
    with model_lock:
        segments, info = model.transcribe(audio, beam_size=5)
        detected_lang_code = info.language

    return detected_lang_code


def write_result(
        result: dict, file: BinaryIO, output: Union[str, None]
):
    if output == "srt":
        WriteSRT(ResultWriter).write_result(result, file=file)
    elif output == "vtt":
        WriteVTT(ResultWriter).write_result(result, file=file)
    elif output == "tsv":
        WriteTSV(ResultWriter).write_result(result, file=file)
    elif output == "json":
        WriteJSON(ResultWriter).write_result(result, file=file)
    elif output == "txt":
        WriteTXT(ResultWriter).write_result(result, file=file)
    else:
        return 'Please select an output method!'


def parse_hotwords(hotwords_str: str) -> str:
    """
    將hotwords字串轉換為標準格式，支援多種分隔符號
    返回以逗號分隔的字串
    """
    if not hotwords_str:
        return ""
        
    # 支援多種分隔符號（逗號、分號、空格）
    separators = [',', ';', ' ']
    words = []
    
    for sep in separators:
        if sep in hotwords_str:
            words = [word.strip() for word in hotwords_str.split(sep) if word.strip()]
            break
    
    if not words:
        words = [hotwords_str.strip()]
        
    return ','.join(words)

def merge_hotwords(*hotwords_sources: Union[str, List[str], None]) -> str:
    """
    合併多個來源的hotwords，去除重複項並保持順序
    返回以逗號分隔的字串
    """
    result = []
    seen = set()
    
    for source in hotwords_sources:
        if source is None:
            continue
            
        if isinstance(source, str):
            words = parse_hotwords(source).split(',') if source.strip() else []
        elif isinstance(source, Iterable):
            words = [str(word) for word in source]
        else:
            continue
            
        for word in words:
            if word and word not in seen:
                seen.add(word)
                result.append(word)
                
    return ','.join(result)
