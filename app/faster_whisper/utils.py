import json
import os
from typing import TextIO

from faster_whisper.utils import format_timestamp


class ResultWriter:
    extension: str

    def __init__(self, output_dir: str):
        self.output_dir = output_dir

    def __call__(self, result: dict, audio_path: str):
        audio_basename = os.path.basename(audio_path)
        output_path = os.path.join(self.output_dir, audio_basename + "." + self.extension)

        with open(output_path, "w", encoding="utf-8") as f:
            self.write_result(result, file=f)

    def write_result(self, result: dict, file: TextIO):
        raise NotImplementedError

class WriteTXT(ResultWriter):
    """文本結果寫入類"""
    extension: str = "txt"
    
    def write_result(self, result: dict, file: TextIO):
        """
        寫入轉錄結果到文本文件
        
        Parameters:
        -----------
        result : dict
            包含轉錄結果的字典，可能是兩種格式：
            1. 原始ASR格式：{"segments": [{"text": "...", "start": 0, "end": 1}, ...]}
            2. 腳本匹配格式：{"text": "...", "segments": [{"text": "...", "start": 0, "end": 1}]}
        file : TextIO
            輸出文件對象
        """
        try:
            # 檢查是否有 segments
            segments = result.get("segments", [])
            
            if segments:
                # 如果 segments 是列表且每個元素都是字典
                if isinstance(segments, list):
                    for segment in segments:
                        # 處理不同的 segment 格式
                        if isinstance(segment, dict):
                            # 新格式：直接使用字典中的 text
                            text = segment.get("text", "").strip()
                        else:
                            # 原始格式：使用 segment 對象的 text 屬性
                            text = segment.text.strip()
                        
                        if text:  # 只輸出非空文本
                            print(text, file=file, flush=True)
            else:
                # 如果沒有 segments，直接輸出整個文本
                text = result.get("text", "").strip()
                if text:
                    print(text, file=file, flush=True)
                    
        except Exception as e:
            # 記錄錯誤並提供回退方案
            logger.error(f"Error writing result: {str(e)}")
            # 嘗試直接輸出文本
            if "text" in result:
                print(result["text"].strip(), file=file, flush=True)
            else:
                raise ValueError("No valid text content found in result")

class WriteVTT(ResultWriter):
    extension: str = "vtt"

    def write_result(self, result: dict, file: TextIO):
        print("WEBVTT\n", file=file)
        for segment in result["segments"]:
            print(
                f"{format_timestamp(segment.start)} --> {format_timestamp(segment.end)}\n"
                f"{segment.text.strip().replace('-->', '->')}\n",
                file=file,
                flush=True,
            )

class WriteSRT(ResultWriter):
    extension: str = "srt"

    def write_result(self, result: dict, file: TextIO):
        for i, segment in enumerate(result["segments"], start=1):
            # write srt lines
            print(
                f"{i}\n"
                f"{format_timestamp(segment.start, always_include_hours=True, decimal_marker=',')} --> "
                f"{format_timestamp(segment.end, always_include_hours=True, decimal_marker=',')}\n"
                f"{segment.text.strip().replace('-->', '->')}\n",
                file=file,
                flush=True,
            )

class WriteTSV(ResultWriter):
    """
    Write a transcript to a file in TSV (tab-separated values) format containing lines like:
    <start time in integer milliseconds>\t<end time in integer milliseconds>\t<transcript text>

    Using integer milliseconds as start and end times means there's no chance of interference from
    an environment setting a language encoding that causes the decimal in a floating point number
    to appear as a comma; also is faster and more efficient to parse & store, e.g., in C++.
    """
    extension: str = "tsv"

    def write_result(self, result: dict, file: TextIO):
        print("start", "end", "text", sep="\t", file=file)
        for segment in result["segments"]:
            print(round(1000 * segment.start), file=file, end="\t")
            print(round(1000 * segment.end), file=file, end="\t")
            print(segment.text.strip().replace("\t", " "), file=file, flush=True)

class WriteJSON(ResultWriter):
    extension: str = "json"

    def write_result(self, result: dict, file: TextIO):
        json.dump(result, file)

from typing import Union, BinaryIO, Tuple, Dict, List
from loguru import logger
class ScriptDatabase:
    """腳本資料庫管理類"""
    def __init__(self, scripts_path: str = "scripts/"):
        self.scripts_path = scripts_path
        self.scripts: Dict[str, List[Dict[str, Dict[str, str]]]] = {}
        self._load_scripts()

    def _load_scripts(self):
        """載入所有腳本"""
        # 確保腳本目錄存在
        os.makedirs(self.scripts_path, exist_ok=True)
        
        # 預設腳本 1 - 高鐵場景
        script1 = {
            "name": "high_speed_rail",
            "scenarios": {
                "zh": {
                    "high_speed_rail_客服1": "您好，您是否需要尋找遺失物中心來幫您尋回物品呢？",
                    "high_speed_rail_客服2": "您可以前往高鐵的服務櫃台，他們會幫助您處理這類遺失物品的問題。",
                    "high_speed_rail_客服3": "沒問題，您從這裡直走，大約50公尺後左轉，您會看到高鐵的服務櫃台就在那邊。"
                },
                "ja": {
                    "high_speed_rail_お客様1": "すみません、私の荷物が見当たらないのですが、どうすればよいでしょうか？",
                    "high_speed_rail_お客様2": "はい、どこに行けばよいですか？",
                    "high_speed_rail_お客様3": "高速鉄道のサービスカウンターへの行き方を教えていただけますか？この辺りに詳しくないもので。",
                }
            }
        }

        # 預設腳本 2 - 機場場景
        script2 = {
            "name": "airport",
            "scenarios": {
                "zh": {
                    "airport_客服1": "當然可以，您是想尋找遺失物中心嗎？這裡有相關的服務。",
                    "airport_客服2": "您可以前往機場服務台，工作人員會幫助您處理遺失物品的問題，並提供必要的協助。",
                    "airport_客服3": "當然可以，您可以直接打給航警局詢問，他們的電話是02-8770-2666。他們會協助查詢遺失的物品。",
                    "airport_客服1_2": "不客氣！如果您有其他問題或需要幫助，隨時可以告訴我們喔！"
                },
                "ja": {
                    "airport_お客様1": "すみません、私の荷物が見当たらないのですが、ご協力いただけますか？",
                    "airport_お客様2": "はい、どうすればよいでしょうか？",
                    "airport_お客様3": "事前に問い合わせできる電話番号はありますか？",
                    "airport_お客様1_2": "ありがとうございます！早速連絡してみます。",
                }
            }
        }

        # 預設腳本 3 - 計程車場景 1
        script3 = {
            "name": "taxi_scenario_1",
            "scenarios": {
                "zh": {
                    "taxi_scenario_1_客服1": "您好！您可以往右前方直走，當您到達出口後，就會看到計程車招呼站，隨時都有計程車在那裡等候。",
                    "taxi_scenario_1_客服2": "一般來說，在招呼站等的話，通常不會等太久，通常在五分鐘內就能搭上車。如果您有急事，也可以考慮使用叫車應用程式，這樣會更方便快捷。",
                    "taxi_scenario_1_客服1_2": "不客氣！如果還有其他問題，隨時都可以來詢問我們喔！"
                },
                "ja": {
                    "taxi_scenario_1_お客様1": "すみません、タクシーはどこで呼べますか？",
                    "taxi_scenario_1_お客様1_2": "分かりました、ありがとうございます！では、これから向かいます。",
                    "taxi_scenario_1_お客様2": "タクシーは通常どのくらい待つ必要がありますか？",
                    "taxi_scenario_1_お客様2_2": "はい、情報ありがとうございます！",
                }
            }
        }

        # 預設腳本 4 - 計程車場景 2
        script4 = {
            "name": "taxi_scenario_2",
            "scenarios": {
                "zh": {
                    "taxi_scenario_2_客服1": "您好！計程車招呼站就在東三門外面，您只需走到門口，右手邊就能看到招呼站。",
                    "taxi_scenario_2_客服1_2": "通常在那裡等幾分鐘就會有計程車過來。如果您急著要走，也可以考慮使用手機叫車的應用程式，會更快。",
                    "taxi_scenario_2_客服2": "不客氣！如果還有其他需要幫忙的，隨時可以來詢問我們喔。"
                },
                "ja": {
                    "taxi_scenario_2_お客様1": "すみません、タクシーはどこで呼べますか？",
                    "taxi_scenario_2_お客様1_2": "ありがとうございます！だいたいどのくらい待つ必要がありますか？",
                    "taxi_scenario_2_お客様2": "アドバイスありがとうございます！試してみます！",
                }
            }
        }

        # 預設腳本 5 - 郵局場景 1
        script5 = {
            "name": "post_office_scenario_1",
            "scenarios": {
                "zh": {
                    "post_office_scenario_1_客服1": "您好！請您往右前方走約兩分鐘，郵局就在您左手邊，沿途會有指示牌，您不會錯過的。",
                    "post_office_scenario_1_客服2": "郵局的營業時間是週一到週五，服務到下午五點。周六則只營業到中午12點，而星期天是不營業的。如果您想寄的文件比較急，建議您今天盡快過去。",
                    "post_office_scenario_1_客服1_2": "是的，郵局不僅提供一般郵寄服務，還有包裹寄送、掛號郵件等多種服務。如果您需要寄送貴重物品，我們建議使用掛號郵件，以確保安全送達。",
                },
                "ja": {
                    "post_office_scenario_1_お客様1": "すみません、郵便局はどこにありますか？手紙を送りたいのですが。",
                    "post_office_scenario_1_お客様1_2": "ありがとうございます！営業時間も教えていただけますか？",
                    "post_office_scenario_1_お客様2": "郵便局は何時まで営業していますか？今日中に送りたいのですが。",
                    "post_office_scenario_1_お客様2_2": "分かりました、ありがとうございます！他にどんなサービスがありますか？例えば荷物の配送や書留などは？",
                    "post_office_scenario_1_お客様1_3": "それは良かったです。書留サービスを検討してみます。ご協力ありがとうございました！"
                }
            }
        }

        # 預設腳本 6 - 郵局場景 2
        script6 = {
            "name": "post_office_scenario_2",
            "scenarios": {
                "zh": {
                    "post_office_scenario_2_客服1": "您好，沒問題，您可以前往東三門或南一門，兩個門附近都有郵局。從這裡走過去大約五分鐘的路程，沿途會有指示牌，不會迷路的。",
                    "post_office_scenario_2_客服1_2": "當然有，您到郵局後，郵局旁邊就有一家便利商店，您可以順便去購買所需的物品。",
                    "post_office_scenario_2_客服2": "郵局週一到週五營業到下午六點半，所以您還有充足的時間。周六則營業到中午12點，請注意，星期天郵局是休息的。如果您有急件需要寄送，建議在週末前處理。",
                },
                "ja": {
                    "post_office_scenario_2_お客様1": "すみません、郵便局はどこにありますか？書類を送りたいのですが。",
                    "post_office_scenario_2_お客様1_2": "ありがとうございます！郵便局の近くにコンビニはありますか？何か買い物もしたいので。",
                    "post_office_scenario_2_お客様2": "それは便利ですね！そうそう、郵便局は何時まで営業していますか？間に合うか心配で。",
                    "post_office_scenario_2_お客様2_2": "分かりました、ご注意いただきありがとうございます！なるべく早く済ませます。"
                }
            }
        }

        # 預設腳本 7 - 捷運場景
        script7 = {
            "name": "mrt_scenario",
            "scenarios": {
                "zh": {
                    "mrt_scenario_客服1": "您好，您可以搭乘捷運紅線，它會直接帶您到台北101站，非常方便。",
                    "mrt_scenario_客服2": "沒問題，您往後方直走，大約100公尺後就能看到捷運站的入口。",
                    "mrt_scenario_客服3": "台北捷運的末班車大約是凌晨12點，建議您提前些時間，避免錯過末班車。"
                },
                "ja": {
                    "mrt_scenario_お客様1": "すみません、台北101へはどう行けばよいですか？",
                    "mrt_scenario_お客様2": "その地下鉄赤線の入口はどこにありますか？少し道に迷ってしまって。",
                    "mrt_scenario_お客様3": "地下鉄は何時まで運行していますか？帰りに間に合うか心配で。",
                }
            }
        }

        # 預設腳本 8 - 觀光卡場景
        script8 = {
            "name": "funpass_scenario",
            "scenarios": {
                "zh": {
                    "funpass_scenario_客服1": "您好，目前我們有6種不同類型的「北北基好玩卡」，每種卡別的優惠內容略有不同，您可以先參考這個網站，裡面有詳細的介紹和比較，讓您更方便選擇。",
                    "funpass_scenario_客服2": "如果您想現場購買，目前可以在市府轉運站的悠遊卡客服中心購買。他們會協助您辦理，並解答您對卡片的任何問題。"
                },
                "ja": {
                    "funpass_scenario_お客様1": "こんにちは、「Taipei FunPASS」を購入したいのですが、購入方法を教えていただけますか？",
                    "funpass_scenario_お客様2": "各カードの違いがよく分からないのですが、直接チケットを購入したいのですが、どうすればよいですか？",
                }
            }
        }

        # 保存所有預設腳本
        scripts = [script1, script2, script3, script4, script5, script6, script7, script8]
        for script in scripts:
            self._save_script(script)
        
        # 載入所有腳本
        for filename in os.listdir(self.scripts_path):
            if filename.endswith('.json'):
                with open(os.path.join(self.scripts_path, filename), 'r', encoding='utf-8') as f:
                    script = json.load(f)
                    self.scripts[script['name']] = script

    def _save_script(self, script: dict):
        """保存腳本到文件"""
        filename = f"{script['name']}.json"
        with open(os.path.join(self.scripts_path, filename), 'w', encoding='utf-8') as f:
            json.dump(script, f, ensure_ascii=False, indent=2)

    def add_script(self, name: str, scenarios: dict):
        """添加新腳本"""
        script = {
            "name": name,
            "scenarios": scenarios
        }
        self._save_script(script)
        self.scripts[name] = script

    def get_all_scripts(self, language: str) -> dict:
        """獲取指定語言的所有腳本內容"""
        all_scripts = {}
        for script in self.scripts.values():
            if language in script['scenarios']:
                all_scripts.update(script['scenarios'][language])
        return all_scripts

import re
from difflib import SequenceMatcher
class FlexibleScriptMatcher:
    def __init__(self, script_db: ScriptDatabase, similarity_threshold: float = 0.8):
        self.script_db = script_db
        self.similarity_threshold = similarity_threshold
        self.keywords_cache = {}
        self._update_keywords()

    def _update_keywords(self):
        """更新關鍵字緩存"""
        for language in ['zh', 'ja']:
            self.keywords_cache[language] = self._extract_keywords(language)

    def _extract_keywords(self, language: str) -> dict:
        """從腳本中提取關鍵字"""
        all_keywords = {
            "zh": {
                "遺失物": ["遺失", "不見", "找回", "丟失", "遺失物中心", "遺失物", "處理"],
                "計程車": ["計程車", "招呼站", "叫車", "等候", "應用程式", "手機叫車", "搭車", "過來"],
                "郵局": ["郵局", "信件", "包裹", "掛號", "文件", "寄送", "貴重物品", "郵寄"],
                "捷運": ["捷運", "捷運站", "紅線", "末班車", "台北101站", "入口", "站"],
                "票卡": ["北北基好玩卡", "悠遊卡", "票", "卡別", "優惠", "FunPASS", "購買"],
                "位置": ["櫃台", "門口", "右手邊", "外面", "走到", "服務台", "左手邊", "東三門", "南一門", "前方", "直走", "公尺", "附近"],
                "時間": ["分鐘", "通常", "等待", "過來", "營業時間", "下午", "中午", "週一", "週五", "星期天", "凌晨", "末班"],
                "服務": ["服務", "協助", "幫助", "辦理", "解答", "諮詢", "購買", "中心"],
                "詢問": ["請問", "不好意思", "謝謝", "好的", "了解", "沒問題"]
            },
            "ja": {
                "遺失物": ["遺失物", "荷物", "見当たらない", "探す", "センター", "対応", "確認"],
                "計程車": ["タクシー", "乗り場", "配車", "アプリ", "待機", "スマートフォン", "乗車"],
                "郵局": ["郵便局", "手紙", "荷物", "書留", "書類", "配達", "貴重品", "郵便"],
                "捷運": ["地下鉄", "駅", "赤線", "最終電車", "台北101駅", "入口", "改札"],
                "票卡": ["北北基好玩卡", "Taipei FunPASS", "チケット", "カード", "特典", "購入"],
                "位置": ["カウンター", "出口", "右手", "外", "進む", "インフォメーション", "左手", "東三門", "南一門", "前方", "まっすぐ", "メートル", "付近"],
                "時間": ["分", "通常", "待つ", "来ます", "営業時間", "午後", "正午", "月曜", "金曜", "日曜", "午前", "最終"],
                "服務": ["サービス", "ご案内", "お手伝い", "手続き", "ご質問", "ご相談", "購入", "センター"],
                "詢問": ["すみません", "ありがとうございます", "はい", "分かりました", "大丈夫です"]
            }
        }
        return all_keywords.get(language, {})

    def _normalize_text(self, text: str) -> str:
        """標準化文本"""
        text = re.sub(r'[^\w\s]', '', text)
        text = re.sub(r'\s+', ' ', text)
        return text.strip()

    def _calculate_similarity(self, text1: str, text2: str) -> float:
        """計算文本相似度"""
        return SequenceMatcher(None, self._normalize_text(text1), self._normalize_text(text2)).ratio()

    def _contains_keywords(self, text: str, keywords: set, threshold: int = 2) -> bool:
        """檢查關鍵字匹配度"""
        return sum(1 for keyword in keywords if keyword in text) >= threshold

    def find_matching_script(self, text: str, language: str) -> Tuple[Union[str, None], float]:
        """查找最匹配的腳本"""
        if language not in self.keywords_cache:
            return None, 0.0

        normalized_input = self._normalize_text(text)
        best_match = None
        best_score = 0.0

        # 從所有腳本中查找最佳匹配
        scripts = self.script_db.get_all_scripts(language)
        for role, script_text in scripts.items():
            normalized_script_text = self._normalize_text(script_text)
            similarity = self._calculate_similarity(normalized_input, normalized_script_text)
            logger.debug(f"比對分數: {similarity}, 正規化文字: {normalized_input}, script_text文字: {script_text}")
            has_keywords = self._contains_keywords(
                normalized_input, 
                self.keywords_cache[language].get(role, set())
            )
            logger.debug(f"是否擁有關鍵字: {has_keywords}")

            if similarity > best_score or has_keywords:
                best_score = similarity
                best_match = script_text

        if best_score >= self.similarity_threshold:
            return best_match, best_score
        return None, best_score