import importlib.metadata
import os
from os import path
from typing import BinaryIO, Union, Annotated, Optional, AsyncGenerator
import time
import ffmpeg
import numpy as np
from fastapi import FastAPI, File, UploadFile, Query, WebSocket, applications
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.responses import StreamingResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from loguru import logger
from .core.tokenizer import LANGUAGES
from .core.logger_config import setup_logger
from urllib.parse import quote
from contextlib import asynccontextmanager
from .core.audio_processor import AudioProcessor
from fastapi.middleware.cors import CORSMiddleware

# 配置 loguru 記錄器
setup_logger()

# 獲取環境變數以確定使用的 ASR 引擎，預設為 faster_whisper
ASR_ENGINE = os.getenv("ASR_ENGINE", "faster_whisper")
MODEL_SIZE = "base"  # 假設模型大小是 large，可以根據實際情況調整

# 根據 ASR_ENGINE 導入相應的 transcribe 和 language_detection 函數
if ASR_ENGINE == "faster_whisper":
    from .faster_whisper.core import transcribe, language_detection
# else:
    # from .openai_whisper.core import transcribe, language_detection

# 定義常數
SAMPLE_RATE = 16000
LANGUAGE_CODES = sorted(list(LANGUAGES.keys()))

# 建立音訊處理器實例
audio_processor = AudioProcessor(sample_rate=SAMPLE_RATE)

# 獲取專案元數據以配置 FastAPI 應用
projectMetadata = importlib.metadata.metadata('whisper-asr-webservice')

@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    FastAPI 應用的 lifespan 事件管理。
    在應用啟動時記錄當前使用的 ASR 引擎和模型大小，並在應用關閉時進行清理工作。

    Parameters:
    ----------
    app: FastAPI
        FastAPI 應用實例。
    """
    logger.info(f"ASR engine: {ASR_ENGINE}")
    logger.info(f"Model size: {MODEL_SIZE}")
    logger.info(f"Server startup.")
    yield
    logger.info("Application shutdown: Cleaning up resources")

app = FastAPI(
    lifespan=lifespan,
    title=projectMetadata['Name'].title().replace('-', ' '),
    description=projectMetadata['Summary'],
    version=projectMetadata['Version'],
    contact={
        "url": projectMetadata['Home-page']
    },
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
    license_info={
        "name": "MIT License",
        "url": projectMetadata['License']
    }
)

# 添加 CORS 支援
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# 設置靜態文件路徑，如果自訂的 swagger-ui 資源存在，則使用這些資源
assets_path = os.getcwd() + "/swagger-ui-assets"
if path.exists(assets_path + "/swagger-ui.css") and path.exists(assets_path + "/swagger-ui-bundle.js"):
    app.mount("/assets", StaticFiles(directory=assets_path), name="static")

    # 修改 FastAPI 預設的 swagger 介面配置
    def swagger_monkey_patch(*args, **kwargs):
        return get_swagger_ui_html(
            *args,
            **kwargs,
            swagger_favicon_url="",
            swagger_css_url="/assets/swagger-ui.css",
            swagger_js_url="/assets/swagger-ui-bundle.js",
        )

    applications.get_swagger_ui_html = swagger_monkey_patch

@app.get("/", response_class=RedirectResponse, include_in_schema=False)
async def index():
    """
    重定向到 API 文件介面。
    """
    return "/docs"

@app.post("/asr", tags=["Endpoints"])
async def asr(
    audio_file: UploadFile = File(...),
    encode: bool = Query(default=True, description="通過 ffmpeg 首先編碼音頻"),
    task: Union[str, None] = Query(default="transcribe", enum=["transcribe", "translate"]),
    language: Union[str, None] = Query(default=None, enum=LANGUAGE_CODES),
    initial_prompt: Union[str, None] = Query(default=None),
    vad_filter: Annotated[bool | None, Query(
        description="啟用語音活動檢測（VAD）以過濾音頻中沒有語音的部分",
        include_in_schema=(True if ASR_ENGINE == "faster_whisper" else False)
    )] = False,
    word_timestamps: bool = Query(default=False, description="單詞級時間戳"),
    hotwords: Union[str, None] = Query(default=None, description="熱詞選項"),
    output: Union[str, None] = Query(default="txt", enum=["txt", "vtt", "srt", "tsv", "json"])
):
    """
    處理音頻文件的 ASR 請求。

    Parameters:
    ----------
    audio_file: UploadFile
        上傳的音頻文件。
    encode: bool
        是否通過 ffmpeg 對音頻進行編碼。
    task: str
        任務類型，可以是 'transcribe' 或 'translate'。
    language: str
        音頻語言代碼。
    initial_prompt: str
        初始提示。
    vad_filter: bool
        是否啟用語音活動檢測（VAD）過濾。
    word_timestamps: bool
        是否啟用單詞級時間戳。
    hotwords: str
        熱詞檢測
    output: str
        輸出文件格式，可以是 'txt', 'vtt', 'srt', 'tsv', 'json'。

    Returns:
    -------
    StreamingResponse
        包含 ASR 結果的流回應。
    """
    start_time = time.time()

    # result = transcribe(load_audio(audio_file.file, encode), task, language, initial_prompt, vad_filter, word_timestamps, hotwords, output)
    # 使用新的處理器處理音訊
    audio_array = await audio_processor.process_file(audio_file.file, encode)
    # 執行辨識
    result = transcribe(
        audio=audio_array,
        task=task,
        language=language,
        initial_prompt=initial_prompt,
        vad_filter=vad_filter,
        word_timestamps=word_timestamps,
        hotwords=hotwords,
        output=output
    )

    execution_time = round(time.time() - start_time, 6)

    return StreamingResponse(
        result,
        media_type="text/plain",
        headers={
            'Asr-Engine': ASR_ENGINE,
            'Content-Disposition': f'attachment; filename="{quote(audio_file.filename)}.{output}"',
            'Execution-Time': str(execution_time)
        }
    )

@app.post("/detect-language", tags=["Endpoints"])
async def detect_language(
    audio_file: UploadFile = File(...),
    encode: bool = Query(default=True, description="通過 FFmpeg 首先編碼音頻")
):
    """
    處理語言檢測請求。

    Parameters:
    ----------
    audio_file: UploadFile
        上傳的音頻文件。
    encode: bool
        是否通過 ffmpeg 對音頻進行編碼。

    Returns:
    -------
    dict
        檢測到的語言及其代碼。
    """
    start_time = time.time()

    # detected_lang_code = language_detection(load_audio(audio_file.file, encode))
    # 使用新的處理器處理音訊
    audio_array = await audio_processor.process_file(audio_file.file, encode)
    detected_lang_code = language_detection(audio_array)

    execution_time = round(time.time() - start_time, 6)
    return {
        "detected_language": LANGUAGES[detected_lang_code], 
        "language_code": detected_lang_code,
        "execution_time": execution_time
    }
