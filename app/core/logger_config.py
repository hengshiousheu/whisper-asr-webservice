# app/logger_config.py
from loguru import logger
import os

def setup_logger():
    # 確保 logs 文件夾存在
    log_folder = "logs"
    os.makedirs(log_folder, exist_ok=True)
    log_file_path = os.path.join(log_folder, "runtime_logs.log")

    # 日志文件配置，包括文件位置、輪轉大小、保留時間等
    logger.add(
        log_file_path, 
        rotation="10 MB", 
        retention="10 days", 
        level="INFO",
        format="{time:YYYY-MM-DD HH:mm:ss ZZ} {level} {message}",
    )