"""
整合版音訊處理模組
結合串流處理和音訊載入功能
"""
import numpy as np
import ffmpeg
from typing import BinaryIO, Union, Optional
from io import BytesIO
from loguru import logger

class AudioProcessor:
    """
    音訊處理器類別
    分別處理檔案和串流的音訊轉換
    """
    def __init__(self, sample_rate: int = 16000):
        self.sample_rate = sample_rate
        self.min_duration_ms = 500  # 最小處理時長（毫秒）
    
    async def process_file(self, audio_file: BinaryIO, encode: bool = True) -> np.ndarray:
        """
        處理完整音訊檔案
        
        參數:
            audio_file: 音訊檔案物件
            encode: 是否需要重新編碼
            
        返回:
            np.ndarray: 處理後的音訊陣列
        """
        try:
            input_data = audio_file.read()
            return await self._process_audio_data(input_data, encode)
        except Exception as e:
            logger.error(f"[AudioProcessor]檔案處理錯誤: {str(e)}")
            raise
    
    async def process_chunk(self, chunk: bytes, encode: bool = True) -> Optional[np.ndarray]:
        """
        處理串流音訊片段
        
        參數:
            chunk: 音訊片段位元組
            encode: 是否需要重新編碼
            
        返回:
            Optional[np.ndarray]: 處理後的音訊陣列，如果片段太小則返回 None
        """
        try:
            # 處理音訊片段
            audio_array = await self._process_audio_data(chunk, encode)
            
            # 檢查音訊長度是否足夠
            duration_ms = len(audio_array) / self.sample_rate * 1000
            if duration_ms < self.min_duration_ms:
                return None
                
            return audio_array
            
        except Exception as e:
            logger.error(f"[AudioProcessor]串流處理錯誤: {str(e)}")
            raise
    
    async def _process_audio_data(self, input_data: bytes, encode: bool) -> np.ndarray:
        """
        打開音頻文件對象並讀取為單聲道波形，必要時重新取樣。
        
        參數:
            input_data: BinaryIO, 音頻文件對象
            encode: 是否需要重新編碼, 如果為 True，通過 ffmpeg 對音頻流進行編碼。
            
        返回:
            np.ndarray: 處理後的音訊陣列。包含音頻波形的 NumPy 陣列，數據類型為 float32。
        """
        try:
            if encode:
                # 使用 ffmpeg 處理音訊
                out, _ = (
                    ffmpeg.input('pipe:', threads=0)
                    .output(
                        '-',
                        format='s16le',
                        acodec='pcm_s16le',
                        ac=1,
                        ar=str(self.sample_rate)
                    )
                    .run(
                        cmd='ffmpeg',
                        capture_stdout=True,
                        capture_stderr=True,
                        input=input_data
                    )
                )
            else:
                out = input_data

            # 轉換為 numpy 陣列
            return np.frombuffer(out, np.int16).flatten().astype(np.float32) / 32768.0

        except ffmpeg.Error as e:
            logger.error(f"[AudioProcessor]FFmpeg 處理錯誤: {e.stderr.decode()}")
            raise RuntimeError(f"[AudioProcessor]音訊處理失敗: {e.stderr.decode()}")
        except Exception as e:
            logger.error(f"[AudioProcessor]音訊處理錯誤: {str(e)}")
            raise