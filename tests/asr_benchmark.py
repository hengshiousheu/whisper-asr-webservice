import os
import pandas as pd
import requests
import soundfile as sf
import time
from io import BytesIO
import logging
import argparse
from tqdm import tqdm
from opencc import OpenCC
import re

# 設定日誌級別
logging.basicConfig(level=logging.INFO)

# 簡體轉繁體的轉換器
cc = OpenCC('s2tw')

def read_audio_file(file_path):
    """
    讀取 WAV 音訊文件並返回二進制數據和取樣率。
    
    :param file_path: 音訊文件的路徑
    :return: 音訊數據和取樣率
    """
    try:
        data, samplerate = sf.read(file_path, dtype='float32')
        return data, samplerate
    except Exception as e:
        logging.error(f"Error reading {file_path}: {e}")
        return None, None

def send_to_asr(file_path, data, samplerate, asr_url, language_key):
    """
    将音频数据发送到 ASR 服务并获取转写结果。
    
    :param file_path: 音频文件的路径，用于获取文件名
    :param data: 音频数据
    :param samplerate: 音频的采样率
    :param asr_url: ASR 服务的 URL
    :param language_key: 语言标识符
    :return: ASR 处理的结果
    """
    try:
        # 获取文件名
        file_name = os.path.basename(file_path)
        
        # 將音訊數據寫入緩衝區
        audio_buffer = BytesIO()
        sf.write(audio_buffer, data, samplerate, format='WAV')
        audio_buffer.seek(0)

        # 構建請求
        files = {'audio_file': (file_name, audio_buffer, 'audio/wav')}
        params = {
            'encode': 'true',
            'task': 'transcribe',
            "language": language_key,
            # 'vad_filter': 'True',
            'word_timestamps': 'false',
            'output': 'txt'
        }
        response = requests.post(asr_url, params=params, files=files, headers={'accept': 'application/json'})

        if response.status_code == 200:
            response_text = response.text
            if language_key.lower() == 'zh':
                response_text = cc.convert(response.text)
            return {'File Path': file_path, 'Prediction': response_text}
        else:
            logging.error(f"Failed to receive valid response from ASR service for file {file_name}. Status code: {response.status_code}")
            return {'File Path': file_path, 'Prediction': 'Error'}
        
    except requests.RequestException as e:
        logging.error(f"Error sending data to ASR with file {file_name}: {e}")
        return {'File Path': file_path, 'Prediction': 'Connection error'}

def process_file(file_path, asr_url, language_key):
    """
    处理单个音频文件的识别。
    
    :param file_path: 音频文件的路径
    :param asr_url: ASR 服务的 URL
    :param language_key: 语言标识符
    :return: 处理结果，包括文件路径、ASR结果和处理时间
    """
    logging.info(f"Processing file: {file_path}")
    data, samplerate = read_audio_file(file_path)
    if data is not None:
        start_time = time.time()
        asr_result = send_to_asr(file_path, data, samplerate, asr_url, language_key)
        execution_time = time.time() - start_time
        return {
            '錄音檔路徑': file_path,
            'ASR執行結果': asr_result['Prediction'],
            '執行時間(s)': execution_time
        }
    else:
        return {'錄音檔路徑': file_path, 'ASR執行結果': 'Error', '執行時間(s)': 'Error'}

def sort_files_numerically(file_list):
    """
    按文件名中的数字部分对文件进行排序。
    
    :param file_list: 文件路径列表
    :return: 排序后的文件路径列表
    """
    def numerical_sort_key(file_path):
        file_name = os.path.basename(file_path)
        numbers = re.findall(r'\d+', file_name)
        return int(numbers[0]) if numbers else float('inf')

    return sorted(file_list, key=numerical_sort_key)

def process_audio_files_sequentially(directory_path, asr_url, language):
    """
    顺序处理資料夾中所有音訊文件。
    
    :param directory_path: 音訊文件所在的資料夾路徑
    :param asr_url: ASR 服务的 URL
    :param language: 语言标识符
    :return: 所有文件的处理结果列表
    """
    audio_files = [os.path.join(directory_path, f) for f in os.listdir(directory_path) if f.endswith(('.wav', '.mp3'))]
    sorted_audio_files = sort_files_numerically(audio_files)
    results = []
    for file_path in tqdm(sorted_audio_files, desc="Processing Audio Files"):
        result = process_file(file_path, asr_url, language)
        results.append(result)

    return results

def save_results_to_csv(results, output_csv_path):
    """
    将结果保存为 CSV 文件。
    
    :param results: 处理结果列表
    :param output_csv_path: 输出 CSV 文件的路径
    """
    results_df = pd.DataFrame(results)
    results_df.to_csv(output_csv_path, encoding='utf-8-sig', index=False)

def main():
    """
    主函数，解析命令行参数并执行音讯文件处理。
    """
    parser = argparse.ArgumentParser(description='Process audio files for ASR.')
    parser.add_argument('--directory_path', required=True, help='Path to the directory containing audio files.')
    parser.add_argument('--asr_url', required=True, help='URL of the ASR service.')
    parser.add_argument('--output_csv', default='asr_results.csv', help='Path where the output CSV will be saved. Default is "asr_results.csv".')
    parser.add_argument('--language', required=True, help='Language code for the ASR service.')

    args = parser.parse_args()
    
    results = process_audio_files_sequentially(args.directory_path, args.asr_url, args.language)
    save_results_to_csv(results, args.output_csv)

if __name__ == '__main__':
    main()
