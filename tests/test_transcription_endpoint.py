# tests/test_transcription_endpoint.py
import pytest
from fastapi.testclient import TestClient
from app.webservice import app
import os 

# 使用 pytest 的 fixture 来创建一个客户端实例，该实例用于发送请求到我们的 FastAPI 应用。
@pytest.fixture(scope="module")
def client():
    return TestClient(app)

# 设置测试用的音频文件路径。
# base_path 获取当前测试文件所在的目录。
base_path = os.path.dirname(__file__)
# assets_path 指向存放测试资源的文件夹，确保测试数据的组织。
assets_path = os.path.join(base_path, "assets")

# 指定具体测试用的音频文件路径。
audio_path = os.path.join(assets_path, "ListeningTest.mp3")

# 定义不同语言的音频文件和期望的语言代码
## "audio_path, expected_language", "language_code"
audio_test_cases = [
    ("tests/assets/en_5.wav", "english", "en"),
    ("tests/assets/zh_5.wav", "chinese", "zh"),
    ("tests/assets/ja_5.wav", "japanese", "ja"),
    ("tests/assets/ko_5.wav", "korean", "ko"),
    ("tests/assets/id_5.wav", "indonesian", "id"),
    ("tests/assets/vi_5.wav", "vietnamese", "vi"),
    ("tests/assets/th_5.wav", "thai", "th"),
]

# 定义一个测试函数，用于测试检测中文的功能。
@pytest.mark.parametrize("audio_path, expected_language, language_code", audio_test_cases)
def test_detect_chinese_language(client, audio_path, expected_language, language_code):
    # 使用 with 语句确保文件在测试后正确关闭。
    with open(audio_path, "rb") as audio:
        # 向指定的 API 端点发送 POST 请求，包括音频文件和其他参数。
        response = client.post(
            "/detect-language",
            files={"audio_file": (audio_path.split('/')[-1], audio, "audio/wav")},
            params={"encode": True}
        )
    # 检查响应状态码是否为 200，确保请求成功。
    assert response.status_code == 200
    # 解析返回的 JSON 数据。
    data = response.json()
    # 检查返回数据中是否包含 'detected_language'。
    assert "detected_language" in data
    # 校验检测到的语言是否为预期的 'chinese'。
    assert data["detected_language"] == expected_language
    # 检查语言代码是否正确为 'zh'。
    assert "language_code" in data
    assert data["language_code"] == language_code
