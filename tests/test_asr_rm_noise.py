# tests/test_asr_endpoint.py
import pytest
from fastapi.testclient import TestClient
from app.webservice import app
import os

# 使用 pytest 的 fixture 来创建一个客户端实例，该实例用于发送请求到我们的 FastAPI 应用。
@pytest.fixture(scope="module")
def client():
    return TestClient(app)

# 准备测试音频文件路径
# base_path = os.path.dirname(__file__)
# assets_path = os.path.join(base_path, "assets", "white_noise")
# audio_file_path = os.path.join(assets_path, "Blender01.mp3")

# 获取音频文件列表
audio_directory = os.path.join(os.path.dirname(__file__), "assets", "white_noise")
audio_files = [os.path.join(audio_directory, f) for f in os.listdir(audio_directory) if f.endswith('.mp3')]

@pytest.mark.parametrize("audio_file_path", audio_files)
def test_asr_endpoint(client, audio_file_path):
    with open(audio_file_path, 'rb') as audio:
        response = client.post(
            "/asr-rm-noise",
            files={"audio_file": (os.path.basename(audio_file_path), audio, "audio/wav")},
            params={
                "encode": True,
                "task": "transcribe",
                "language": "zh",
                "initial_prompt": "",
                "vad_filter": False,
                "word_timestamps": False,
                "output": "txt"
            }
        )
        assert response.status_code == 200
        assert response.headers['Content-Type'] == "text/plain; charset=utf-8"
        assert response.headers['Asr-Engine'] == "faster_whisper"
        assert response.text == ""
        # 检查响应体是否包含预期的内容，这将取决于你的具体实现
        # assert "expected text or data" in response.content

# 更多测试函数可以添加在这里，测试不同的参数组合或错误情况
